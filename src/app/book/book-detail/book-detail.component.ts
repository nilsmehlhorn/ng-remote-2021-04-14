import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EMPTY, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { BookApiService } from '../book-api.service';
import { Book } from '../models/book';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.scss'],
})
export class BookDetailComponent implements OnInit {
  book$: Observable<Book> = EMPTY;

  constructor(private route: ActivatedRoute, private bookApi: BookApiService) {}

  ngOnInit(): void {
    // DON'T SUBSCRIBE INSIDE SUBSCRIBE
    // this.route.params.subscribe(({params}) => {
    //   const isbn = params.isbn
    //   this.bookApi.getBookByIsbn(isbn).subscribe((bookFromApi) => {
    //     this.book = bookFromApi;
    //   });
    // })
    this.book$ = this.route.params.pipe(
      map((params) => params.isbn),
      switchMap((isbn) => this.bookApi.getBookByIsbn(isbn))
    );
  }
}

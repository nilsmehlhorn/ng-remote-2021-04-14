import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Book } from '../models/book';

@Component({
  selector: 'app-book-new',
  templateUrl: './book-new.component.html',
  styleUrls: ['./book-new.component.scss'],
})
export class BookNewComponent implements OnInit {
  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      title: ['', [Validators.required]],
      isbn: ['', [Validators.required, Validators.maxLength(11)]]
    })
  }

  ngOnInit(): void {}

  onSubmit(): void {
    const {title, isbn} = this.form.value as Partial<Book>
    console.log(this.form.value)
    // TODO: send to API with service
  }
}

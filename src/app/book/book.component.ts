import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BookApiService } from './book-api.service';
import { Book } from './models/book';
import { delay, share } from 'rxjs/operators'
import { Router } from '@angular/router';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss'],
})
export class BookComponent implements OnInit, OnDestroy {
  searchTerm = '';

  books$: Observable<Book[]> | undefined;

  constructor(private bookApi: BookApiService, private router: Router) {}

  ngOnInit(): void {
    this.books$ = this.bookApi.getAll().pipe(delay(3000));
  }

  ngOnDestroy(): void {
    // if (this.subscription) {
    //   this.subscription.unsubscribe();
    // }
    // this.subscription?.unsubscribe();
    // this.destroy$.next();
  }

  goToDetails(book: Book): void {
    console.log(`Navigate to ${book.isbn}`);
    this.router.navigate(['book', 'details', book.isbn])
  }

  updateSearchTerm(value: string): void {
    this.searchTerm = value;
  }
}

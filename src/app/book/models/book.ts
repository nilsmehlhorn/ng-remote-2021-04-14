export interface Book {
  isbn: string;
  cover: string;
  title: string;
  author: string;
  abstract: string;
  numPages: number;
  publisher: {
    name: string;
    url: string;
  }
}

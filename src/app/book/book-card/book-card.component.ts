import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Book } from '../models/book';

@Component({
  selector: 'app-book-card',
  templateUrl: './book-card.component.html',
  styleUrls: ['./book-card.component.scss'],
})
export class BookCardComponent implements OnInit {
  @Input()
  content: Book | undefined;

  @Output()
  detailClick = new EventEmitter<Book>();

  constructor() {}

  ngOnInit(): void {}

  handleDetailClick(event: MouseEvent): void {
    event.preventDefault();
    this.detailClick.emit(this.content);
  }
}

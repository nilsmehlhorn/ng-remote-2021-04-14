import { Pipe, PipeTransform } from '@angular/core';
import { Book } from '../models/book';

@Pipe({
  name: 'bookFilter'
})
export class BookFilterPipe implements PipeTransform {
  transform(books: Book[] | null, searchTerm: string): Book[] {
    if (!books) {
      return [];
    }
    if (!searchTerm) {
      return books;
    }
    const loweredSearchTerm = searchTerm.toLowerCase();
    return books.filter(
      ({ title, abstract }) =>
        title.toLowerCase().includes(loweredSearchTerm) ||
        abstract.toLowerCase().includes(loweredSearchTerm)
    );
  }
}
